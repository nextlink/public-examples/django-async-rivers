from unittest import mock

from django.test import TestCase
from django.urls import reverse

from .views import SITES


class RiverDashboardTests(TestCase):
    @mock.patch('rivers.views.get_river_flow_and_height')
    def test_dashboard_v1(self, mock_get_river_flow_and_height):
        response = self.client.get(reverse('rivers:dashboard_v1'))

        self.assertTemplateUsed(response, 'rivers/dashboard.html')
        self.assertEqual(response.context['version'], 'v1')

        mock_get_river_flow_and_height.assert_has_calls([
            mock.call(site_id)
            for site_id in SITES
        ])

    @mock.patch('rivers.views.get_river_flow_and_height')
    def test_dashboard_v2(self, mock_get_river_flow_and_height):
        response = self.client.get(reverse('rivers:dashboard_v2'))

        self.assertTemplateUsed(response, 'rivers/dashboard.html')
        self.assertEqual(response.context['version'], 'v2')

        mock_get_river_flow_and_height.assert_has_calls([
            mock.call(site_id)
            for site_id in SITES
        ], any_order=True)

    @mock.patch('rivers.views.get_river_flow_and_height_async')
    def test_dashboard_v3(self, mock_get_river_flow_and_height_async):
        response = self.client.get(reverse('rivers:dashboard_v3'))

        self.assertTemplateUsed(response, 'rivers/dashboard.html')
        self.assertEqual(response.context['version'], 'v3')

        mock_get_river_flow_and_height_async.assert_has_calls([
            mock.call(site_id)
            for site_id in SITES
        ], any_order=True)
