import asyncio
import collections
import concurrent.futures
import time

import httpx
import requests

from django.shortcuts import render


SITES = collections.OrderedDict({
    '06192500': 'Livingston',
    '06214500': 'Billings',
    '06295000': 'Forsyth',
    '06309000': 'Miles City',
    '06327500': 'Glendive',
    '06329500': 'Sidney',
})


VARIABLES = {
    '00060': 'Discharge, cubic feet per second',
    '00065': 'Gage height, feet',
}


def parse_flow_and_height_from_json(json_data):
    flow_and_height_data = {
        '00060': None,
        '00065': None,
    }
    for item in json_data['value']['timeSeries']:
        variable_code = item['variable']['variableCode'][0]['value']
        value = item['values'][0]['value'][0]['value']
        if variable_code in flow_and_height_data:
            flow_and_height_data[variable_code] = float(value)
    return flow_and_height_data


def get_river_flow_and_height(site_id):
    """
    Synchronous method to get river data from USGS
    """
    response = requests.get(f'https://waterservices.usgs.gov/nwis/iv/?format=json&sites={site_id}&parameterCd=00060,00065&siteStatus=all')
    data = parse_flow_and_height_from_json(response.json())
    return data


async def get_river_flow_and_height_async(site_id):
    """
    Asynchronous method to get river data from USGS
    """
    async with httpx.AsyncClient() as client:
        response = await client.get(f'https://waterservices.usgs.gov/nwis/iv/?format=json&sites={site_id}&parameterCd=00060,00065&siteStatus=all')
        data = parse_flow_and_height_from_json(response.json())
    return data


def dashboard_v1(request):
    """
    Synchronous view that loads data one at a time
    """
    start_time = time.time()

    river_data = []

    for site_id in SITES.keys():
        river_data.append((SITES[site_id], get_river_flow_and_height(site_id)))

    return render(request, 'rivers/dashboard.html', {
        'river_data': river_data,
        'version': 'v1',
        'load_time': time.time() - start_time,
    })


def dashboard_v2(request):
    """
    Concurrent view that loads some data simultaneously
    """
    start_time = time.time()

    river_data = []

    with concurrent.futures.ThreadPoolExecutor(max_workers=6) as executor:
        for site_id, data in zip(SITES.keys(), executor.map(get_river_flow_and_height, SITES.keys())):
            river_data.append((SITES[site_id], data))

    return render(request, 'rivers/dashboard.html', {
        'river_data': river_data,
        'version': 'v2',
        'load_time': time.time() - start_time,
    })


async def dashboard_v3(request):
    """
    Asynchronous view that loads data using asyncio
    """
    start_time = time.time()

    river_data = []

    datas = await asyncio.gather(*[get_river_flow_and_height_async(site_id) for site_id in SITES.keys()])
    for site_id, data in zip(SITES.keys(), datas):
        river_data.append((SITES[site_id], data))

    return render(request, 'rivers/dashboard.html', {
        'river_data': river_data,
        'version': 'v3',
        'load_time': time.time() - start_time,
    })
